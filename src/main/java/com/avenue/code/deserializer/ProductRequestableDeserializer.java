package com.avenue.code.deserializer;

import java.io.IOException;

import com.avenue.code.model.ProductCreateRequest;
import com.avenue.code.model.ProductRequestable;
import com.avenue.code.model.ProductUpdateRequest;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ProductRequestableDeserializer extends JsonDeserializer<ProductRequestable> {
	
    @Override
    public ProductRequestable deserialize(JsonParser jp,  DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode root = (ObjectNode) mapper.readTree(jp);
        Class<? extends ProductRequestable> instanceClass = null;
               
        if (isUpdatable(root.toString())) {
        	instanceClass = ProductUpdateRequest.class;
        } else if(isEmptyObject(root.toString())){
        	return null;
        } else {
        	instanceClass = ProductCreateRequest.class;
        }
        
        return mapper.readValue(root.toString(), instanceClass);
    }
    
    private boolean isUpdatable(final String json) {
    	return json.contains("id");
    }
    
    private boolean isEmptyObject(final String json) {
    	return json.equals("{}");
    }
}
