package com.avenue.code.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product implements Serializable {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String name;
	
	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE } )
	@JoinColumn(name = "PARENT_PRODUCT_ID", nullable = true)
	private Product parentProduct;
	
	@OneToMany
	private List<Image> images;
	
	public Product() {}
	
	public Product(final String name) {
		this.name = name;
	}
	
	public Product(final Long id, final String name) {
		this(name);
		this.id = id;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(final String name) {
		this.name = name;
	}
	
	public Product getParentProduct() {
		return this.parentProduct;
	}
	
	public void setParentProduct(final Product parentProduct) {
		this.parentProduct = parentProduct;
	}
	
	public List<Image> getImages() {
		return this.images;
	}
	
	public void setImages(final List<Image> images) {
		this.images = images;
	}

	private static final long serialVersionUID = 3282594166914533440L;
}
