package com.avenue.code.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Image implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	@Lob
	private byte[] picture;
	private String description;
	
	public Image() {}
	
	public Image(final byte[] picture, final String description) {
		this.picture = picture;
		this.description = description;
	}
	
	public Image(final Long id, final byte[] picture, final String description) {
		this(picture, description);
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	private static final long serialVersionUID = -1523375628613800048L;
}
