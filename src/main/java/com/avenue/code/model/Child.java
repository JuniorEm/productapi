package com.avenue.code.model;

import java.io.Serializable;

public class Child implements Serializable {

	private Long id;
	private String productName;
	
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
		
	public String getProductName() {
		return this.productName;
	}
	
	public void setProductName(final String productName) {
		this.productName = productName;
	}
	
	private static final long serialVersionUID = 3283561609692286061L;
}
