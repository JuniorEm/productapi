package com.avenue.code.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = ProductUpdateRequest.class)
public class ProductUpdateRequest implements ProductRequestable {

	private Long id;
	private String name;
	private ProductUpdateRequest parentProduct;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public ProductRequestable getParentProduct() {
		return this.parentProduct;
	}
	@Override
	public void setParentProduct(ProductRequestable parentProduct) {
		this.parentProduct = (ProductUpdateRequest) parentProduct;
	}

	private static final long serialVersionUID = -8852797628380580128L;
}
