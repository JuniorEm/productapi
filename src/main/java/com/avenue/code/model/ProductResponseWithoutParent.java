package com.avenue.code.model;

import java.io.Serializable;

public class ProductResponseWithoutParent implements Serializable {

	private Long id;
	private String name;
	
	public ProductResponseWithoutParent() {}
	
	public ProductResponseWithoutParent(final Long id, final String name) {
		this.id = id;
		this.name = name;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	private static final long serialVersionUID = 3641760121560671959L;
}
