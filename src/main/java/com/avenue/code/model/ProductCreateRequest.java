package com.avenue.code.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = ProductCreateRequest.class)
public class ProductCreateRequest implements ProductRequestable {
	
	private String name;
	private ProductRequestable parentProduct;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public ProductRequestable getParentProduct() {
		return parentProduct;
	}

	@Override
	public void setParentProduct(final ProductRequestable parentProduct) {
		this.parentProduct = parentProduct;
	}

	private static final long serialVersionUID = 8670538071317236766L;
}
