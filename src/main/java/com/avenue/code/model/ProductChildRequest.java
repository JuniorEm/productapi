package com.avenue.code.model;

import java.io.Serializable;
import java.util.List;

public class ProductChildRequest implements Serializable {

	private Long parentId;
	private List<Child> children;
	
	public Long getParentId() {
		return parentId;
	}
	
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	public List<Child> getChildren() {
		return children;
	}
	
	public void setChildren(List<Child> children) {
		this.children = children;
	}
	
	private static final long serialVersionUID = 7090897066261951138L;
}
