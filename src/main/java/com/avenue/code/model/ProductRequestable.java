package com.avenue.code.model;

import java.io.Serializable;

import com.avenue.code.deserializer.ProductRequestableDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = ProductRequestableDeserializer.class)
public interface ProductRequestable extends Serializable {
	
	String getName();
	void setName(String name);
	ProductRequestable getParentProduct();
	void setParentProduct(final ProductRequestable parentProduct);
	
}
