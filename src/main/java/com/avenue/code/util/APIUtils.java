package com.avenue.code.util;

import java.util.Arrays;

import com.avenue.code.entity.Product;
import com.avenue.code.model.ProductRequestable;

public class APIUtils {

	public static boolean isProductNotNull(final Product product) {
		return !isProductNull(product);
	}

	public static boolean isProductNull(final Product product) {
		return isNull(product);
	}
	
	public static boolean isProductNotNull(final ProductRequestable product) {
		return !isProductNull(product);
	}
	
	public static boolean isProductNull(final ProductRequestable product) {
		return isNull(product);
	}
	
	public static boolean isNull(final Object obj) {
		return obj == null;
	}
	
	public static boolean isNotNull(final Object obj) {
		return !isNull(obj);
	}
}
