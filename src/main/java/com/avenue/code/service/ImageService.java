package com.avenue.code.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.avenue.code.entity.Image;
import com.avenue.code.entity.Product;
import com.avenue.code.repository.ImageRepository;
import com.avenue.code.repository.ProductRepository;
import com.avenue.code.service.exception.NoDataFoundException;

@org.springframework.stereotype.Service
public class ImageService implements Service<Image, Long> {

	static final String NO_RESULT_NO_IMAGES_FOUND = "No result, no images found";
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Image create(final Image model) {
		return this.imageRepository.save(model);
	}
	
	public Image create(final Image model, final Long productId) {
		final Product found = Optional.ofNullable(this.productRepository.findOne(productId)).orElseThrow(() -> new NoDataFoundException(ProductService.NO_RESULT_FOUND_FOR_PRODUCT));

		final Image saved = this.create(model);
		found.getImages().add(saved);
		this.productRepository.save(found);
		
		return saved;
	}

	@Override
	public Image update(final Image model) {
		this.findOne(model.getId());
		return this.imageRepository.save(model);
	}

	@Override
	public int delete(Long key) {
		final Image found = this.imageRepository.findOne(key);
		this.imageRepository.delete(this.findOne(found.getId()));
		return HttpStatus.OK.value();
	}
	
	public Image getImageByProductId(final Long id) {
		return this.imageRepository.findByProductId(id);
	}
	
	public Image findOne(final Long id) {
		return Optional.ofNullable(this.imageRepository.findOne(id)).orElseThrow(() -> new NoDataFoundException(NO_RESULT_NO_IMAGES_FOUND));
	}
}
