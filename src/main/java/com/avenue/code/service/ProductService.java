package com.avenue.code.service;

import static com.avenue.code.util.APIUtils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;

import com.avenue.code.entity.Product;
import com.avenue.code.model.Child;
import com.avenue.code.model.ProductChildRequest;
import com.avenue.code.repository.ProductRepository;
import com.avenue.code.service.exception.CannotDeleteAggregateRegisterException;
import com.avenue.code.service.exception.NoDataFoundException;
import com.avenue.code.validator.Validator;

@org.springframework.stereotype.Service
public class ProductService implements Service<Product, Long> {

	static final String NO_RESULT_FOUND_FOR_PRODUCT = "No result found for product";
	static final String ERROR_AFTER_CREATE = "After create the result didnt retrieve any result";

	@Autowired
	private ProductRepository productRepository;

	@Qualifier("productNameValidator")
	@Autowired
	private Validator<Product> productNameValidator;

	@Override
	public Product create(final Product product) {
		this.setNullForParentProductRecursively(product);
		this.saveRecursively(product);

		return this.findOneWithErrorAfterCreateMessage(product.getId());
	}

	@Override
	public Product update(final Product product) {
		this.verifyExistenceRecursivelyForUpdate(product);
		this.setNullForParentProductRecursively(product);
		this.updateRecursively(product);

		return this.findOne(product.getId());
	}

	@Override
	public int delete(final Long id) {
		try {
			this.productRepository.delete(this.findOne(id));
			return HttpStatus.OK.value();
		} catch (final DataIntegrityViolationException e) {
			throw new CannotDeleteAggregateRegisterException(
					"Cannot delete 'cause the register is aggregated as a Foreign Key");
		}
	}

	public List<Product> findAllWithoutChildrenAssociation() {
		return this.productRepository.findAllWithoutChildrenAssociation();
	}

	public Product findByIdWithoutChildrenAssociation(final Long id) {
		return this.productRepository.findByIdWithoutChildrenAssociation(id);
	}

	public List<Product> findAllWithChildrenAssociation() {
		return this.productRepository.findAllWithChildrenAssociation();
	}

	public Product findByIdWithChildrenAssociation(final Long id) {
		return this.productRepository.findByIdWithChildrenAssociation(id);
	}

	public List<Product> findChildProductsOfAParentProductById(final Long id) {
		return this.productRepository.findByIdAllParentRelationship(id);
	}
	
	public List<Product> setAllChildrenForAParent(final ProductChildRequest productChildRequest) {
		final Product parent = this.findOne(productChildRequest.getParentId());
		
		final List<Product> products = new ArrayList<>();
		
		productChildRequest.getChildren().forEach(c -> {
			Product found = new Product();
			
			if (isNull(c.getId())) {
				found = this.getProductByChild(c);
			} else {
				found = this.findOne(c.getId());
			}
			
			found.setParentProduct(parent);
			this.productRepository.save(found);
			products.add(found);
		});
		
		return products;
	}
	
	private Product getProductByChild(final Child child) {
		final Product product = new Product();
		product.setName(child.getProductName());
		return product;
	}

	private void verifyExistenceRecursivelyForUpdate(final Product product) {

		if (isProductNotNull(product)) {
			final Product found = this.productRepository.findOne(product.getId());
			Optional.ofNullable(found).orElseThrow(() -> new NoDataFoundException(NO_RESULT_FOUND_FOR_PRODUCT));
			this.verifyExistenceRecursivelyForUpdate(product.getParentProduct());
		}
	}

	private void saveRecursively(final Product product) {

		if (isProductNotNull(product)) {

			if (isProductNotNull(product.getParentProduct())) {
				final Product parentProduct = product.getParentProduct();
				final Product foundParentProduct = this.productRepository
						.findParentProductByName(parentProduct.getName());

				if (isProductNull(foundParentProduct)) {
					final Product saved = this.productRepository.save(parentProduct);
					product.setParentProduct(saved);
				} else {
					product.setParentProduct(foundParentProduct);
				}
			}

			this.productRepository.save(product);

			this.saveRecursively(product.getParentProduct());
		}
	}

	private void setNullForParentProductRecursively(final Product product) {

		if (isProductNotNull(product)) {
			if (isProductNotNull(product.getParentProduct()) && isNull(product.getParentProduct().getName())) {
				product.setParentProduct(null);
			}
			setNullForParentProductRecursively(product.getParentProduct());
		}
	}

	private void updateRecursively(final Product product) {

		if (isProductNotNull(product)) {
			this.productRepository.save(product);
			this.updateRecursively(product.getParentProduct());
		}
	}
	
	public Product findOne(final Long id) {
		return Optional.ofNullable(this.productRepository.findOne(id)).orElseThrow(() -> new NoDataFoundException(NO_RESULT_FOUND_FOR_PRODUCT));
	}
	
	public Product findOneWithErrorAfterCreateMessage(final Long id) {
		return Optional.ofNullable(this.productRepository.findOne(id)).orElseThrow(() -> new NoDataFoundException(ERROR_AFTER_CREATE));
	}
}
