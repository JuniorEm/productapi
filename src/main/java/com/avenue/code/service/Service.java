package com.avenue.code.service;

public interface Service<M, K> {

	M create(final M model);
	M update(final M model);
	int delete(final K key);
}
