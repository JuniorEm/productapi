package com.avenue.code.service.exception;

public class CannotDeleteAggregateRegisterException extends RuntimeException {

	public CannotDeleteAggregateRegisterException() {
		
	}
	
	public CannotDeleteAggregateRegisterException(final String msg) {
		super(msg);
	}
	
	public CannotDeleteAggregateRegisterException(final String msg, final Throwable cause) {
		super(msg, cause);
	}
	
	private static final long serialVersionUID = 7309081680815143533L;
}
