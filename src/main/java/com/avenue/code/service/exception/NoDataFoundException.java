package com.avenue.code.service.exception;

public class NoDataFoundException extends RuntimeException {
	
	public NoDataFoundException(final String message) {
		super(message);
	}
	
	public NoDataFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
	private static final long serialVersionUID = -7318871422636038804L;
}
