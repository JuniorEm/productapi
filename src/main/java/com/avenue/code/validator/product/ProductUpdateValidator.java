package com.avenue.code.validator.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.avenue.code.entity.Product;
import com.avenue.code.validator.Validator;

@Component
public class ProductUpdateValidator implements Validator<Product> {

	@Qualifier("nullValidator")
	@Autowired
	private Validator<Product> nullValidator;
	
	@Qualifier("productNameValidator")
	@Autowired
	private Validator<Product> productNameValidator;
	
	@Qualifier("idValidator")
	@Autowired
	private Validator<Long> idValidator;
	
	@Override
	public void validate(final Product model) {
		this.nullValidator.validate(model);
		this.idValidator.validate(model.getId());
		Assert.notNull(model.getName(), "The name of the product cannot be null");
	}
}
