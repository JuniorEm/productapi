package com.avenue.code.validator.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.avenue.code.entity.Product;
import com.avenue.code.validator.Validator;

@Component
public class ProductCreateValidator implements Validator<Product> {
	
	@Qualifier("productNameValidator")
	@Autowired
	private Validator<Product> productNameValidator;
	
	@Override
	public void validate(final Product model) {
		Assert.isNull(model.getId(), "The id must be null to create a new Product");
		Assert.notNull(model.getName(), "The name of the product cannot be null");
		this.productNameValidator.validate(model);
	}
}
