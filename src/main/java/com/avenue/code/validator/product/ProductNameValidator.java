package com.avenue.code.validator.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.avenue.code.entity.Product;
import com.avenue.code.repository.ProductRepository;
import com.avenue.code.validator.Validator;

@Component
public class ProductNameValidator implements Validator<Product> {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public void validate(Product model) {
		Assert.isNull(this.productRepository.findByName(model.getName()), "There is a product with this name!");
	}

}
