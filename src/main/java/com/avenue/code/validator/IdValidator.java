package com.avenue.code.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class IdValidator implements Validator<Long> {
	
	private static final Long ZERO_ID = 0l;

	@Override
	public void validate(Long id) {
		Assert.isTrue(isNotEqualsToZeroAndGreaterThanZero(id), "The ID cannot be negative or equals to ZERO");
		Assert.notNull(id, "The passed id cannot be null");
	}

	private boolean isNotEqualsToZeroAndGreaterThanZero(Long id) {
		return !id.equals(ZERO_ID) && id > ZERO_ID;
	}
}
