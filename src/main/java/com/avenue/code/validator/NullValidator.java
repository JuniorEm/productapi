package com.avenue.code.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@SuppressWarnings("rawtypes")
@Component
public class NullValidator implements Validator {

	@Override
	public void validate(Object model) {
		Assert.notNull(model, "The passed model Product, cannot be null");
	}
}
