package com.avenue.code.validator;

public interface Validator<M> {

	void validate(final M model);
	
}
