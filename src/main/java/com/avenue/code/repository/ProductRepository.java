package com.avenue.code.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenue.code.entity.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

	Product findByName(final String name);
	
	@Query("select new Product(p.id, p.name) from Product p")
	List<Product> findAllWithoutChildrenAssociation();
	
	@Query("select p from Product p")
	List<Product> findAllWithChildrenAssociation();
	
	@Query("select new Product(p.id, p.name) from Product p where p.id = ?1")
	Product findByIdWithoutChildrenAssociation(final Long id);
	
	@Query("select p from Product p where p.id = ?1")
	Product findByIdWithChildrenAssociation(final Long id);
	
	@Query("select p from Product p where p.parentProduct.id = ?1")
	Product findByParentProductId(final Long id);

	@Query("select new Product(p.id, p.name) from Product p where p.parentProduct.id = ?1")
	List<Product> findByIdAllParentRelationship(final Long id);
	
	@Query("select p from Product p where p.name = ?1")
	Product findParentProductByName(final String name);
}
