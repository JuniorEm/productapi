package com.avenue.code.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.avenue.code.entity.Image;

@Repository
public interface ImageRepository extends CrudRepository<Image, Long> {
	
	@Query("select i from Product p JOIN p.images i where p.id = ?1")
	Image findByProductId(final Long id);
}