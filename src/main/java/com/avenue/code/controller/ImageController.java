package com.avenue.code.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.avenue.code.entity.Image;
import com.avenue.code.service.ImageService;
import com.avenue.code.validator.Validator;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/image")
public class ImageController {
	
	@Autowired
	private ImageService imageService;
	
	@Qualifier("idValidator")
	@Autowired
	private Validator<Long> idValidator;

	@RequestMapping(method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(value = "Persist an image", notes = "Persist an image with passed model")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Image create(@RequestPart(required = true) @Valid @ApiParam(value = "File to upload", required = true) 
						final MultipartFile multipartFile,
						final Long productId,
						final String description) {
		
		try {
			final Image image = new Image(multipartFile.getBytes(), description);
			final Image created = this.imageService.create(image, productId);
			
			return created;
		} catch (final IOException e) {
			throw new RuntimeException("There is an error in input bytes file");
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	@ApiOperation(value = "Update an image", notes = "Update an image with passed model")
	@ResponseStatus(value = HttpStatus.OK)
	public Image update(@RequestPart(required = true) @Valid @ApiParam(value = "File to upload", required = true) 
						final MultipartFile multipartFile, final Long id, final String description) {

		try {
			final Image image = new Image(id, multipartFile.getBytes(), description);
			final Image updated = this.imageService.update(image);
			
			return updated;
		} catch (final IOException e) {
			throw new RuntimeException("There is an error in input bytes file");
		}		
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public int delete(@PathVariable("id") final Long id) {
		this.idValidator.validate(id);
		return this.imageService.delete(id);
	}
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "Get an image", notes = "Get an image by product id")
	@ResponseStatus(value = HttpStatus.OK)
	public Image getImageByProductId(@PathVariable("id") final Long id) {
		return this.imageService.getImageByProductId(id);
	}
}
