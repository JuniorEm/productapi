package com.avenue.code.controller;

import static com.avenue.code.util.APIUtils.*;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.avenue.code.entity.Product;
import com.avenue.code.model.ProductChildRequest;
import com.avenue.code.model.ProductCreateRequest;
import com.avenue.code.model.ProductRequestable;
import com.avenue.code.model.ProductResponseWithoutParent;
import com.avenue.code.model.ProductUpdateRequest;
import com.avenue.code.service.ProductService;
import com.avenue.code.validator.Validator;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Qualifier("productCreateValidator")
	@Autowired
	private Validator<Product> productCreateValidator;
	
	@Qualifier("productUpdateValidator")
	@Autowired
	private Validator<Product> productUpdateValidator;
	
	@Qualifier("idValidator")
	@Autowired
	private Validator<Long> idValidator;
	
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(value = "Creates a product", notes = "Creates a product with passed model")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Product create(@RequestBody @Valid final ProductCreateRequest productCreateRequest) {
		
		final Product product = new Product();
		
		this.setRecursiveParentProduct(product, productCreateRequest);		
		this.productCreateValidator.validate(product);
		
		return this.productService.create(product);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	@ApiOperation(value = "Update a product", notes = "Update a specific product by passed object. In this case we can update the parent and the child."
			+ "If the id of parent is invalid, an error will be throw, the same will be occur to child")
	@ResponseStatus(value = HttpStatus.OK)
	public Product update(@RequestBody @Valid final ProductUpdateRequest productUpdateRequest) {
		
		final Product product = new Product();
		
		this.setRecursiveParentProduct(product, productUpdateRequest);
		this.productUpdateValidator.validate(product); 
		
		final Product updated = this.productService.update(product);		
		return updated;
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Delete a product", notes = "Delete a specific product by passed id")
	public int delete(@PathVariable("id") final Long id) {
		this.idValidator.validate(id);
		return this.productService.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Get all product list", notes = "Get all product list without their parent product")
	public List<ProductResponseWithoutParent> findAllWithoutParentAssociation() {
		final List<ProductResponseWithoutParent> newList = new ArrayList<>();
		
		this.productService.findAllWithoutChildrenAssociation().forEach(p -> {
			newList.add(new ProductResponseWithoutParent(p.getId(), p.getName()));
		});
		
		return newList;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Get specific product list", notes = "Get a specific product (found by id) without their child products")
	public ProductResponseWithoutParent findByIdWithoutChildrenAssociation(@PathVariable("id") final Long id) {
		final Product found = this.productService.findByIdWithoutChildrenAssociation(id);
		
		return new ProductResponseWithoutParent(found.getId(), found.getName());
	}
	
	@RequestMapping(value = "/parent", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Get all product list", notes = "Get all product list with theirs parent products and relationship")
	public List<Product> findAllWithParentAssociation() {
		return this.productService.findAllWithChildrenAssociation();
	}
	
	@RequestMapping(value = "/parent/{id}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Get a specific product list with parent product", notes = "Get a specific product (found by id) with their parent product")
	public Product findByIdWithParentAssociation(@PathVariable("id") final Long id) {
		return this.productService.findByIdWithChildrenAssociation(id);
	}
	
	@RequestMapping(value = "/children/{id}/parent", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	@ApiOperation(value = "Get all children of a parent product", notes = "Get all children by parent product id")
	public List<Product> findChildProductsOfAParentProductById(@PathVariable("id") final Long id) {
		return this.productService.findChildProductsOfAParentProductById(id);
	}
	
	@RequestMapping(value = "/children", method = RequestMethod.POST)
	@ApiOperation(value = "Set all children of a specific parent", notes = "You can add several children for one parent. "
			+ "It is valid to remember: you can create the relationship in this method, if the child product doesn't exist, we will create it, "
			+ "on the other hand, if the product exists, we will find it by id on database and ignore the name that you passed (if you passed) and "
			+ "we'll create the relationship")
	@ResponseStatus(value = HttpStatus.OK)
	public List<Product> setAllChildrenForAParent(@Valid @RequestBody final ProductChildRequest productChildRequest) {
		return this.productService.setAllChildrenForAParent(productChildRequest);
	}
	
	private void setRecursiveParentProduct(final Product product, final ProductRequestable mainProduct) {
		
		product.setName(mainProduct.getName());
		
		if (mainProduct instanceof ProductUpdateRequest) {
			ProductUpdateRequest productUpdateRequest = (ProductUpdateRequest) mainProduct;
			product.setId(productUpdateRequest.getId());
		}
		
		if (isProductNotNull(mainProduct.getParentProduct())) {
			
			final Product parentProduct = new Product(mainProduct.getParentProduct().getName());
			
			if (isNull(mainProduct.getParentProduct().getName())) {
				product.setParentProduct(null);
			} else {
				product.setParentProduct(parentProduct);
			}
			
			final ProductRequestable nextMainProduct = mainProduct.getParentProduct();
			this.setRecursiveParentProduct(parentProduct, nextMainProduct);
		}
	}
}
